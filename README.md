# dka-mka, dlp2019func

#### spusteni: dka-2-mka volba [vstup]
* kde vstup je jmeno vstupniho souboru (pokud neni specifikovano,
program cte standardni vstup) obsahujiciho DKA v zadanem formatu.
* volba je parametr ovlivnujici chovani programu:
	* -i: Dojde pouze k vypsani nacteneho DKA na stdout v zadanem formatu.
	* -t: Dojde k vypsani MKAtot na stdout v zadanem formatu.

#### Textova reprezentace DKA na vstupu:
* <seznam vsech stavu>\n
* <pocatecni stav>\n
* <seznam koncovych stavu>\n
* <pravidlo 1>\n
* ...
* <pravidlo N>\n

#### Textova reprezentace MKA na vystupu:
* Stejna jako na vstupu ale plati i nasledujici body formatu navic.
* Stavy MKA cislujte souvisle od nuly a vypisujte je vzestupne serazene.
* Vstupni stav bude vzdy 0.
* Koncove stavy take seradte.
* Pravidla seradte lexikograficky podle vychoziho stavu a symbolu abecedy.
* Cilovy stav kazdeho pravidla smi byt nanejvys o 1 vetsi nez nejvetsi cilovy stav z pravidel nad nim.
 
