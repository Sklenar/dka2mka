#!/usr/bin/python3.6

import re
import sys
import contextlib
import io
from os.path import exists
import tempfile
import errno

def isWritable(path):
	try:
		testfile = tempfile.TemporaryFile(dir = path)
		testfile.close()
	except OSError as e:
		return 0
		raise
		
	return 1
#------------------------------------------------------------------------------------------------------------------------------	
def smartWrite(filename,wStr):
	strPath=""
	path=""
	
	if (filename) and (filename != '-'):
		path=filename.split("/")
		path=path[:-1]
		
		for c in path:
			strPath+=c+"/"
			
		#print(strPath)
		if not(isWritable(strPath)):
			errLog(14)
	
		fh = open(filename, 'w', newline='')
	else:
		fh = sys.stdout
	
	fh.write(wStr)
	
	if (fh is not sys.stdout):
		fh.close()
#------------------------------------------------------------------------------------------------------------------------------	
def errLog(err):
	exCode=0

	if (err==0):
		exCode=60	
		print("Chyba zavorek!", file=sys.stderr)
	elif (err==1):
		exCode=60
		print("Chyba zanoreni!", file=sys.stderr)
	elif (err==2):
		exCode=61
		print("Automat neni validni, nebyla provedena minimalizace!", file=sys.stderr)
	elif (err==3):
		exCode=1
		print("Byl zadan neplatny argument!", file=sys.stderr)
	elif (err==4):
		exCode=1
		print("Help lze zadat pouze samostatne!", file=sys.stderr)
	elif (err==5):
		exCode=1
		print("Nebyla nalezena hodnota pro argument(arg=hodnota)!", file=sys.stderr)
	elif (err==6):
		exCode=2
		print("Zadany vstupni soubor nebyl nalezen!", file=sys.stderr)
	elif (err==7):
		exCode=3
		print("Zadany vstupni soubor nelze otevrit!", file=sys.stderr)
	elif (err==8):
		exCode=1
		print("Nelze kombinavat argument -m(--minimize) s -f(--find-non-finishing)!", file=sys.stderr)
	elif (err==9):
		exCode=62
		print("Byl nalezen nedosazitelny stav!", file=sys.stderr)
	elif (err==10):
		exCode=1
		print("Nelze kombinavat argument --analyze-string s -m(--minimize) nebo s -f(--find-non-finishing)!", file=sys.stderr)
	elif (err==11):
		exCode=1
		print("Pismeno z --analyze-string neni ve vstupni abecede ka!", file=sys.stderr)
	elif (err==12):
		exCode=1
		print("V argumentech byla nalezena duplicita!", file=sys.stderr)
	elif (err==13):
		exCode=60
		print("Chyba pri zpracovani souboru!", file=sys.stderr)
	elif (err==14):
		exCode=3
		print("Do zadaneho vystupniho souboru nelze zapisovat!", file=sys.stderr)
	else:
		exCode=60
		print("Neznama chyba!", file=sys.stderr)
	

	if (exCode>0): exit(exCode)	
#------------------------------------------------------------------------------------------------------------------------------	
def makeArrFromStr(str,opr):
	global wChar
	
	#print(str)
	#print(len(str))
	
	index=0
	lst=[]
	pomStr=""
	count=0
	
	for c in str:
		#print(index)
		
		if(c=="'"):count+=1
		
		if (opr) and (count%2==0):
			if not(wChar) and (len(pomStr)!=0) and (index+1<len(str)-1) and (count!=0) and (str[index+1]!=','):
				return lst
			#if(index>0):print(str[index-1])
			
			if(len(pomStr)!=0): lst.append(pomStr)
			pomStr=""
			index+=1
			continue
		
		if (opr) or not(((c==" ") or (c==",") or (c=="'") or (c=="{") or (c=="}")) and not((index>0 and index<len(str)-1) and ((str[index-1]=="'") and (str[index+1]=="'")))):			
			if (opr) and (c=="'") and (count%2==0):
				pomStr+=c
			elif (opr) and (c!="'"):
				pomStr+=c
			elif not(opr):
				pomStr+=c
				
		elif (((wChar) and (c==" ")) or (c==",") or (c=="}")) and not((index>0 and index<len(str)-1) and ((str[index-1]=="'") and (str[index+1]=="'"))):				
			if(len(pomStr)!=0): lst.append(pomStr)
			pomStr=""
			
		index+=1
	
	#print(lst)
	
	return lst
#------------------------------------------------------------------------------------------------------------------------------		
def makeArrLstFromStr(str):
	global rulesOnly
	global wChar

	#print(str)
	
	index=0
	lst=[]
	pom=[]
	pomStr=""
	#and not((index>0 and index<len(str)-1) and ((str[index-1]=="'") and (str[index+1]=="'")))
	for c in str:
		if not(((c==" ") or (c==",") or (c=="'") or (c=="-") or (c==">") or (c=="{") or (c=="}")) and not((index>0 and index<len(str)-1) and ((str[index-1]=="'") and (str[index+1]=="'")))):
			pomStr+=c
			#print(c)
		elif (((wChar) and (c==" ")) or (c==",") or (c=="'") or (c=="-") or (c=="}")) and (pomStr!=""):				
			pom.append(pomStr)			
			pomStr=""
			
		if (((wChar) and (c==" ")) or (c==",") or (c=="}")) and (len(pom)==3):			
			#print(pom)
			lst.append(pom);pom=[]
		
		index+=1
		
	if (rulesOnly):
		pom.append(pomStr)
		lst.append(pom)
	
	#print(*lst,',')
	
	return lst
#------------------------------------------------------------------------------------------------------------------------------		
def removeDupl(lst):
	seen = set()
	result = []
	
	for item in lst:
		if item not in seen:
			seen.add(item)
			result.append(item)
	
	return result
#------------------------------------------------------------------------------------------------------------------------------	
def removeDuplKa(ka):
	#print(*ka, sep=",")

	lst=[ka[0]]
	dupl=0	
	
	for n,o,g in ka:		
		for n2,o2,g2 in lst:
			if (equalStrings(n,n2)) and (equalStrings(o,o2)) and (equalStrings(g,g2)):
				dupl=1
				break
		
		if not dupl:
			lst.append([n,o,g])
				
		dupl=0
	return lst	
#------------------------------------------------------------------------------------------------------------------------------	
def minKa(ka,goalNodes):
	#print(ka)
	
	pomNode=""
	lst=[]
	p=1
	str="_"
	pom=[]
	pomKeys=[]
	kst=[]
	pomIndex=0
	
	for startN,op,goalN in ka:
		p=1
		for startN2,op2,goalN2 in ka:			
			if (equalStrings(startN,startN2)) and (equalStrings(op,op2)) and (equalStrings(goalN,goalN2)): continue			
			
			if(pomNode!=startN2) and not(p):
				startN2=pomNode
				break
			else:
				pomNode=startN2
			
			if (equalStrings(op,op2)) and (equalStrings(goalN,goalN2)):				
				p=0
			else:
				if ((equalStrings(startN2,goalN)) and (equalStrings(startN,goalN2)) and (equalStrings(op,op2))):	
						for n,o,g in ka:
							#op==op2
							if not(equalStrings(o,op)) or not(equalStrings(n,startN)):continue
							
							if(pomNode!=startN2) and not(p):
								startN2=pomNode
								break
							else:
								pomNode=startN2
							
							if (equalStrings(o,op2)) and (equalStrings(goalN,g)):				
								p=0;	
							else:
								p=1
								
		if not (p):
			if (ekvNodes(startN,startN2,goalNodes)):
				if not(equalStrings(startN,startN2)):
					pom.append(startN)
					pom.append(startN2)
				
					pom.sort()
					pomKeys.append(pom[0]+str+pom[1])
				
					break
	
	
	pom = removeDupl(pom)
	pomKeys = removeDupl(pomKeys)
	
	if(len(pomKeys)>0):pomKeys=sortNodes(pomKeys)
	
	#print(*pom, sep=',')
	#print(*pomKeys, sep=',')
	
	for startN,op,goalN in ka:
		p=1
		for key in pom:						
			if(equalStrings(key,startN)):p+=5
			if(equalStrings(key,goalN)):p+=6
					
			pomIndex=finRepKey(key,pomKeys)
			
			
		if(p==1):
			kst.append([startN,op,goalN])
		elif(p==6):
			kst.append([pomKeys[pomIndex],op,goalN])
		elif(p==7):
			kst.append([startN,op,pomKeys[pomIndex]])
		elif(p==12):
			kst.append([pomKeys[pomIndex],op,pomKeys[pomIndex]])
	
	kst=removeDuplKa(kst)
	
	if compareLists(ka,kst):		
		return 0
	else:
		return kst
#------------------------------------------------------------------------------------------------------------------------------
def ekvNodes(n1,n2,gNodes):
	ret=1
	gn1=0
	gn2=0
	
	for n in gNodes:
		if(equalStrings(n,n1)):
			gn1=1
		
		if(equalStrings(n,n2)):
			gn2=1
	
	if (gn1!=gn2):
		ret=0
		
	return ret
#------------------------------------------------------------------------------------------------------------------------------
def sortNodes(keys):
	ret=[]
	ret.append("")
	lst=[]
	str='_'
	pom=keys[0]
	index=0
	
	lst=pom.split(str)
	lst.sort()
	
	for n in lst:
		if(index!=0): ret[0]+=str
		ret[0]+=n
		index+=1
	
	return ret
#------------------------------------------------------------------------------------------------------------------------------	
def compareLists(a,b):	
	r=[[x for x in a if x not in b], [x for x in b if x not in a]]
	
	if(len(r[0])==0):	
		return 1
	else:
		return 0
	
#------------------------------------------------------------------------------------------------------------------------------
def finRepKey(k,keys):
	index=0

	for n in keys:		
		if((k) in n):
			return index
		
		index+=1
#------------------------------------------------------------------------------------------------------------------------------	
def getNodes(ka):
	lst=[]
	
	for startN,op,goalN in ka:
		lst.append(startN)
		lst.append(goalN)
	
	lst = removeDupl(lst)
		
	return lst
#------------------------------------------------------------------------------------------------------------------------------	
def replaceNodes(nodes,nodesK):
	ret=[]
	rep=0
	str="_"
	
	#print(nodes)
	#print(nodesK)
	
	for n in nodes:
		for node in nodesK:			
			if(n in node) and (str in node):
				rep=1
				break
			elif(equalStrings(n,node)):
				rep=0;break
		
		if(rep):
			ret.append(node)
		else:
			ret.append(n)
			
		rep=0
			
	ret = removeDupl(ret)
	
	return ret
#------------------------------------------------------------------------------------------------------------------------------
def	getPrintOprs(opr):
	ret="{"
	index=0
	
	for c in opr:
		if index<1:
			ret+=("'"+c+"'")
		else:
			ret+=(", '"+c+"'")
	
		index+=1
	
	return ret+"}"	
#------------------------------------------------------------------------------------------------------------------------------
def	getPrintNodes(nodes):
	ret="{"
	index=0
	
	for n in nodes:
		if index<1:
			ret+=(n)
		else:
			ret+=(", "+n)
	
		index+=1

	return ret+"}"
#------------------------------------------------------------------------------------------------------------------------------
def getPrintKa(ka):
	ret="{\r\n"
	index=0
	
	for startN,op,goalN in ka:
		if index<1:
			ret+=(startN+" '"+op+"' "+"-> "+goalN)
		else:
			ret+=(",\r\n"+startN+" '"+op+"' "+"-> "+goalN)
	
		index+=1

	return ret+"\r\n}"
#------------------------------------------------------------------------------------------------------------------------------
def findUnreachableNode(nodes,startNode,ka):
	ret=0
	
	for c in nodes:
		if ret==1: break
	
		if (equalStrings(c,startNode)): continue
		
		ret=1
		for n,o,g in ka:
			if (equalStrings(c,g)) and not(equalStrings(c,n)): ret=0;break
		
	#print(c+" "+str(ret))
	return ret
#------------------------------------------------------------------------------------------------------------------------------
def validateKa(nodes,operations,startNode,goalNodes,ka):
	ret=1
	pom=0
	pomNode=""
	pomOp=[]
	
	#kontrola toho ze pocatecni ma vyskyt v mnozine stavu	
	for n in nodes:
		if (equalStrings(startNode,n)): 
			pom=1
			break
		
	if not(pom): 
		ret=0
		return ret
	
	pom=0
	#kontrola toho ze pocatecni stav ma v pravidlech aspon jeden vyskyt		
	for n,o,g in ka:
		if (equalStrings(startNode,n)): 
			pom=1
			break
		
	if not(pom): 
		ret=0
		return ret
	
	pom=0
	#kontrola toho ze kazdy stav ma jenom jednu moznost pro jeden prechod		
	for n,o,g in ka:
		if(pomNode==""):pomNode=n
		
		if equalStrings(pomNode,n):
			pomOp.append(o)
		else:		
			if(len(pomOp) != len(set(pomOp))):
				pom=1
				break
			
			pomNode=n
			pomOp=[]
		
	if (pom): 
		ret=0
		return ret
	
	pom=0
	#kontrola toho ze aspon jeden z konecnych stavu ma v pravidlech aspon jeden vyskyt		
	for node in goalNodes:
		if(equalStrings(node,startNode)):
			pom=1
	
	if not(pom):
		pom=0
		for node in goalNodes:
			for n,o,g in ka:			
				if (equalStrings(node,g)): 
					pom=1
					break
			
			if not(pom):break
			
		if not(pom): 
			ret=0
			return ret
	
	pom=0	
	#kontrola toho ze pocatecni stavy pravidel jsou z mnoziny stavu
	for n,o,g in ka:
		pom=0
		
		for c in nodes:
			if (equalStrings(c,n)): 
				pom=1
				break
		
		if pom==0: break
		
	if not(pom): 
		ret=0
		return ret
	
	pom=0
	#kontrola toho ze cilove stavy pravidel je z mnoziny stavu
	for n,o,g in ka:
		pom=0
		for c in nodes:
			if (equalStrings(g,c)): 
				pom=1
				break
		
		if pom==0: break
		
	if not(pom): 
		ret=0
		return ret
		
	pom=0
	#kontrola toho ze operace pravidel je z mnoziny pravidel
	for n,o,g in ka:
		pom=0
		for c in operations:
			if (equalStrings(o,c)): 
				pom=1
				break
		
		if pom==0: break
		
	if not(pom): 
		ret=0
		return ret
	
	pom=0
	#kontrola toho ze mnozina koncovych stavu  je podmnozinou mnoziny vsech stavu
	for n in goalNodes:
		pom=0
		for c in nodes:
			if (equalStrings(n,c)): 
				pom=1
				break
		
		if pom==0: break
		
	if not(pom): 
		ret=0
		return ret
	
	#kontrola duplikaci
	#if (len(nodes) != len(set(nodes))): ret=0;return ret
	#if (len(operations) != len(set(operations))): ret=0;return ret
	#if (len(goalNodes) != len(set(goalNodes))): ret=0;return ret
	
	#kontrola prazdnosti mnozin
	if (len(nodes)==0): ret=0;return ret
	if (len(goalNodes)==0): ret=0;return ret
	if (len(operations)==0): ret=0;return ret
	
	return ret
#------------------------------------------------------------------------------------------------------------------------------
def printHelp():
	help=""
	
	help+="\n"
	
	help+="--help vypise napovedu ke skriptu"
	help+="\n"
	
	help+="--input=filename zpracuje obsah souboru filename, misto STDIN"
	help+="\n"
	
	help+="--output=filename vytiskne vysledny retezec do souboru filename, misto STDOUT"
	help+="\n"
	
	help+="-f, --find-non-finishing skript najde neukoncujici stav"
	help+="\n"
	
	help+="-m, --minimize skript provede minimalizaci ka, pred jeho vypsanim"
	help+="\n"
	
	help+="-i, --case-insensitive skript si nebude vsimat rozdilu mezi velkym a malym pismem"
	help+="\n"
	
	help+="-r, --rules-only skript prijme zkraceny zapis ka na vstupu (pouze pravidla)"
	help+="\n"
	
	help+="--analyze-string=\"retezec\" skript proveri zda ka zpracuje zadany retezec"
	help+="\n"
	
	help+="-w, --white-char vse muze byt oddeleno bilym znakem misto ','"
	help+="\n"
		
	
	print(help)
#------------------------------------------------------------------------------------------------------------------------------
def findNonFinishing(ka):
	pom=0
	pomNode=""
	k=0
	
	for n,o,g in ka:
		if(equalStrings(pomNode,n)) and (k==1): continue
		
		if not(equalStrings(pomNode,n)):
			if(pom==1):break
			pom=0
			k=0
			pomNode=n
		
		if not(equalStrings(pomNode,g)): pom=0;k=1
		else: pom=1
	
	if not(pom):
		for n,o,g in ka:
			k=0
			for n2,o2,g2 in ka:
				if (equalStrings(n,n2)) and (equalStrings(o,o2)) and (equalStrings(g,g2)): continue
				
				if(g==n2): k=1;break
			
			if not(k):
				pom=1
				pomNode=g
	
	if(pom): return pomNode
	else: return str(pom)
#------------------------------------------------------------------------------------------------------------------------------
def getNodesRO(ka):
	lst=[]
	pomStr=""
	
	for n,o,g in ka:
		for c in n:
			if (c!="."):
				pomStr+=c
				
		lst.append(pomStr)
		pomStr=""
		for r in g:
			if (r!="."):
				pomStr+=r
				
		lst.append(pomStr)
		pomStr=""
		
	lst = removeDupl(lst)
	
	return lst
#------------------------------------------------------------------------------------------------------------------------------
def getOpRO(ka):
	lst=[]
	
	for n,o,g in ka:
		lst.append(o)
		
	lst = removeDupl(lst)
	
	return lst
#------------------------------------------------------------------------------------------------------------------------------
def getGoalNodesRO(ka):
	ret=[]
	lst=[]
	pomStr=""
	finishNode=0
	
	for n,o,g in ka:
		for c in g:
			if(c=="."): finishNode=1
		
		if finishNode:
			lst.append(g)
			finishNode=0
		
	for g in lst:		
		for r in g:
			if (r!="."):
				pomStr+=r
		
		ret.append(pomStr)
		pomStr=""
	
	
	ret = removeDupl(ret)
	
	return ret
#------------------------------------------------------------------------------------------------------------------------------
def getFirstRO(ka):	
	return ka[0][0]
#------------------------------------------------------------------------------------------------------------------------------
def reduceDots(ka):
	lst=[]
	pom=[]
	pomStr=""
	
	for n,o,g in ka:
		for c in n:
			if (c!="."):
				pomStr+=c
				
		pom.append(pomStr)
		pomStr=""
		
		pom.append(o)
		
		for r in g:
			if (r!="."):
				pomStr+=r
				
		pom.append(pomStr)
		pomStr=""		
	
		lst.append(pom)
		pom=[]
	
	return lst
#------------------------------------------------------------------------------------------------------------------------------
def analyzeString(analString,ka,operations,goalNodes,startNode):
	ret=0
	fail=1
	acNode=startNode
	
	for c in analString:
		for n in operations:
			if(equalStrings(n,c)):fail=0
		
		if(fail):
			errLog(11)
		
		fail=1
	
	for c in analString:
		ret=0
			
		for n,o,g in ka:
			if (equalStrings(acNode,n)) and (equalStrings(c,o)):
				acNode=g
				ret=1
				break
		
		if not(ret):
			break
	
	if (ret):
		ret=0
		for g in goalNodes:
			if(equalStrings(g,acNode)):ret=1;break
	
	
	return str(ret)
#------------------------------------------------------------------------------------------------------------------------------
def removeSpaces(str):
	ret=re.sub('[\s+]', '', str)
	
	return ret
#------------------------------------------------------------------------------------------------------------------------------
def equalStrings(s1,s2):
	pomS1=s1[:]
	pomS2=s2[:]	
	
	if(pomS1==pomS2):
		return 1
	else:
		return 0	
#------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------

	
minKautomat=0	
fileNameIn=""
fileNameOut="-"
pomIndex=0
fnf=0
caseInsens=0
rulesOnly=0
analString=""
wChar=0

pomArgv=removeDupl(sys.argv[1:])

#print(pomArgv)
#print(sys.argv[1:])

if (len(pomArgv)!=len(sys.argv[1:])):
	errLog(12)

for arg in sys.argv[1:]:
	arg=arg.split("=")
	
	if(arg[0]=="--help"):
		if(pomIndex!=0) or (len(sys.argv[1:])>1): errLog(4)
		
		printHelp()
		exit(0)
	elif(arg[0]=="--input"):
		if(arg[1]==""): errLog(5)
		
		fileNameIn=arg[1]
	elif(arg[0]=="--output"): 
		if(arg[1]==""): errLog(5)
	
		fileNameOut=arg[1]
	elif(arg[0]=="-f") or (arg[0]=="--find-non-finishing"):
		fnf=1
	elif(arg[0]=="-m") or (arg[0]=="--minimize"):
		minKautomat=1
	elif(arg[0]=="-i") or (arg[0]=="--case-insensitive"):
		caseInsens=1
	elif(arg[0]=="-r") or (arg[0]=="--rules-only"):
		rulesOnly=1		
	elif(arg[0]=="--analyze-string"): 
		if(arg[1]==""): errLog(5)
	
		analString=arg[1]	
	elif(arg[0]=="-w") or (arg[0]=="--white-char"):
		wChar=1		
	else:
		errLog(3)
	
	pomIndex+=1
	
	
if (analString!="") and ((minKautomat) or (fnf)):
	errLog(10)
	
if(minKautomat) and (fnf):
	errLog(8)
	
if(fileNameIn!=""):	
	if(exists(fileNameIn)):
		try:
			f = open(fileNameIn, 'r')
		except:
			errLog(7)
	else:
		errLog(6)
else:
	f=input('')
#---------------------------------------------konecZpracovaniArgumentu--------------------------------------		
counterCollatBrackets=0
counterSimpleBrackets=0

input=""
rawInput=[]

for line in f:	
	if line[0]!="#":
		rawInput.append(line)

if(caseInsens):
	index=0
	for n in rawInput:
		rawInput[index]=rawInput[index].lower()
		index+=1
		
if isinstance(f, io.IOBase):
	f.close()
		
for line in rawInput:
	index=0
	for c in line:		
		if ((c=='#') or (c=='(') or (c==')') or (c=='\r\n')) and not((index>0 and index<len(line)-1) and ((line[index-1]=="'") and (line[index+1]=="'"))): 
			if ((c=='(') or (c==')')) and not((index>0 and index<len(line)) and ((line[index-1]=="'") and (line[index+1]=="'"))):counterSimpleBrackets+=1
			input=input+' ';break
		
		if (c!='\r\n'): 
			input=input+c
		if ((c=='{') or (c=='}')) and not((index>0 and index<len(line)-1) and ((line[index-1]=="'") and (line[index+1]=="'"))): 
			counterCollatBrackets+=1	
		
		index+=1

if not(wChar):
	input=re.sub('[\s+]', '', input)
else:
	input=re.sub(',', ' ', input)
	input=" ".join(input.split())

#print(input)
#print(counterSimpleBrackets)
#print(counterCollatBrackets)

if (not rulesOnly) and ((counterSimpleBrackets!=2) or (counterCollatBrackets!=8)):
	errLog(0)
elif(rulesOnly) and ((counterSimpleBrackets!=0) or (counterCollatBrackets!=0)):
	errLog(0)
	
index2=0	
depth=0
min=0
nodes=""
operations=""
ka=""
startNode=""
goalNodes=""
resultString=""

index=0
if not(rulesOnly):
	for c in input:	
		if ((c=="{")) and not((index>0 and index<len(input)-1) and ((input[index-1]=="'") and (input[index+1]=="'"))): 
			depth+=1
		elif ((c=="}")) and not((index>0 and index<len(input)-1) and ((input[index-1]=="'") and (input[index+1]=="'"))):			
			depth-=1
		
		index+=1
		
		if (depth==0) and ((c==",") or ((c==" ") and (wChar))):
			index2+=1 
			continue
		
		
		if (index2==0):nodes=nodes+c
		elif (index2==1):operations=operations+c
		elif (index2==2):ka=ka+c
		elif (index2==3):startNode=startNode+c
		elif (index2==4):goalNodes=goalNodes+c
		else:errLog(1);
			
	nodesL=makeArrFromStr(nodes,0)		
	operationsL=makeArrFromStr(operations,1)
	goalNodesL=makeArrFromStr(goalNodes,0)
	startNodeL=removeSpaces(startNode)
	kaL=makeArrLstFromStr(ka)
else:
	for c in input:	
		if ((c=="{")) and not((index>0 and index<len(input)-1) and ((input[index-1]=="'") and (input[index+1]=="'"))): depth+=1
		elif ((c=="}")) and not((index>0 and index<len(input)-1) and ((input[index-1]=="'") and (input[index+1]=="'"))): depth-=1
		
		index+=1
		
		if (depth!=0): errLog(1)
		
		ka=ka+c
		
	kaL=makeArrLstFromStr(ka)
		
	if (len(kaL[0])!=3):
		errLog(13)
	
	nodesL=getNodesRO(kaL)		
	operationsL=getOpRO(kaL)
	goalNodesL=getGoalNodesRO(kaL)
	startNodeL=getFirstRO(kaL)
	kaL=reduceDots(kaL)
	
#print("N:"+nodes+"\n"+"Op:"+operations+"\n"+"KA:"+ka+"\n"+"sN:"+startNode+"\n"+"gN:"+goalNodes+"\n")		

if (startNodeL=="") or (len(nodesL)<1) or (len(operationsL)<1) or (len(kaL)<1) or (len(goalNodesL)<1):
	errLog(13)

nodesL=removeDupl(nodesL)
operationsL=removeDupl(operationsL)
kaL=removeDuplKa(kaL)
goalNodesL=removeDupl(goalNodesL)

nodesL.sort()
operationsL.sort()
kaL.sort()
goalNodesL.sort()

#print(*nodesL, sep='\n')	
#print("-----------------------------")
#print(*operationsL, sep='\n')
#print("-----------------------------")
#print(*kaL, sep='\n')
#print("-----------------------------")
#print(startNodeL)
#print("-----------------------------")
#print(*goalNodesL, sep='\n')

valid=validateKa(nodesL,operationsL,startNodeL,goalNodesL,kaL)

if not(valid):
	if (findUnreachableNode(nodesL,startNodeL,kaL)): errLog(9)
	
if minKautomat:
	kaR=kaL	
	
	while(minKa(kaR,goalNodesL)!=0):
		kaR=minKa(kaR,goalNodesL)
		min=1
	
	if(min):
		nodesR=getNodes(kaR)
		nodesR.sort()
	else:
		nodesR=nodesL
else:
	kaR=kaL
	nodesR=nodesL

#print(*kaR, sep='\n')

if fnf:
	resultString=findNonFinishing(kaL)

if not(valid): 
	errLog(2)
elif not(fnf):	
	operationsR=operationsL
	
	if(min):
		startNodeR=replaceNodes(startNodeL,nodesR)[0]
		goalNodesR=replaceNodes(goalNodesL,nodesR)
		
		goalNodesR.sort()
	else:
		startNodeR=startNodeL
		goalNodesR=goalNodesL

	if (analString!=""):
		resultString=analyzeString(analString,kaR,operationsR,goalNodesR,startNodeR)
	else:
		resultString="(\r\n"
		resultString+=getPrintNodes(nodesR)+",\r\n"
		resultString+=getPrintOprs(operationsR)+",\r\n"
		resultString+=getPrintKa(kaR)+",\r\n"
		resultString+=startNodeR+",\r\n"
		resultString+=getPrintNodes(goalNodesR)+"\r\n"
		resultString+=")"
		resultString+="\r\n"
		
#------------------------------------------------------------------------------------------------------------------------------	
smartWrite(fileNameOut,resultString)	
#------------------------------------------------------------------------------------------------------------------------------	
exit(0)

