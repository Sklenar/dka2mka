--FLP2019fun, dka-mka
--autor: Zdenek Sklenar, xsklen09

import System.Environment
import System.Directory
import System.Exit

import Data.List
import Debug.Trace (trace)

--trace("" ++ show [to,co,chci,vypsat])(fejkovani vystupu fce stejny jako ma normalne, kvuli prekladaci)
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--chybove funkce
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--chybova funkce pro spatny pocet argumentu
invalidArg :: IO b
invalidArg =do
  error "Invalid arguments. dka-2-mka [-i,-t] {inFile}"
  exitWith (ExitFailure 42)
----------------------------------------------------------------------------------------------------
--chybova funkce pro neplatnou cestu
fileNotExist :: String -> IO b
fileNotExist f=do
  error "inFile does not exists! "::f
  exitWith (ExitFailure 21)
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--radici funkce
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--radici funkce pro mnoziny s nazvy uzlu, ktere jsou cisla
sortByFirstNode :: [String] -> [String] -> Ordering
sortByFirstNode (arr1) (arr2)
  | a < b = LT
  | a > b = GT
  | a == b = LT --je to jedno
  where
    a=(convertStrToInt (arr1!!0))
    b=(convertStrToInt (arr2!!0))
----------------------------------------------------------------------------------------------------
--radici funkce pro pravidla, podle zadani
sortDelta :: [Char] -> [Char] -> Ordering
sortDelta (a1) (a2)
  | a < b = LT
  | a > b = GT
  | a == b = compare (d1!!1) (d2!!1)
  where
    d1=(splt a1 ',')
    d2=(splt a2 ',')
    a=(convertStrToInt (d1!!0))
    b=(convertStrToInt (d2!!0))
----------------------------------------------------------------------------------------------------
--radici funkce pro nazvy uzlu, ktere jsou cisla
sortNodes :: [Char] -> [Char] -> Ordering
sortNodes (a1) (a2)
  | a < b = LT
  | a > b = GT
  | a == b = LT --je to jedno
  where
    a=(convertStrToInt a1)
    b=(convertStrToInt a2)
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--nahrazovaci funkce
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--
repl find r [] = []
repl find r (x:xs) =
    if find == x
      then
        r : (repl find r xs)
      else
        x : (repl find r xs)
----------------------------------------------------------------------------------------------------
--
repl2 [] [] x = x
repl2 (f:fs) (r:rs) x =
    if f == x
      then
        r
      else
        (repl2 fs rs x)
----------------------------------------------------------------------------------------------------
--
replFl [] [] arr vys =vys
replFl (find:fs) (r:rs) arr vys =
    if(isMember find arr)
      then
        if((length arr) == 1)
          then
            (replFl fs rs lastE (r:vys))
          else
            (replFl fs rs rest (r:vys))
      else
        (replFl fs rs arr vys)
    where
      rest=tail arr
      lastE=[head arr]
----------------------------------------------------------------------------------------------------
--
rp find rep a =
    replFl find rep [a] []
----------------------------------------------------------------------------------------------------
--
replFl2 find rep arr =
    (rp find rep (arr!!0))++(rp find rep (arr!!1))++(rp find rep (arr!!2))
----------------------------------------------------------------------------------------------------
--
replF find rep [] vys = vys
replF find rep (a:arr) vys =
    replF find rep arr ((repl2 find rep a):vys)
----------------------------------------------------------------------------------------------------
--
replFe [] [] endNodes = endNodes
replFe (n:ns) (p:ps) endNodes =
    replFe ns ps (rsEs n p endNodes)
----------------------------------------------------------------------------------------------------
--
rsEs n [] nodes = nodes
rsEs n (r:rs) nodes =
    rsEs n rs (repl r n nodes)
----------------------------------------------------------------------------------------------------
--jednosmerne nahrazeni uzlu v mnozine delta
replFS :: [String] -> [[Char]] -> [String] -> [[Char]] -> [[Char]]
replFS find replace [] vys = vys
replFS find replace (a:arr) vys =
    replFS find replace arr ((intercalate "," (replFl2 find replace (splt a ','))):vys)
----------------------------------------------------------------------------------------------------
--obousmerne nahrazeni uzlu v mnozine delta
replFSe :: String -> String -> [String] -> [[Char]] -> [[Char]]
replFSe find replace [] vys = vys
replFSe find replace (a:arr) vys =
    replFSe find replace arr (intercalate "," ((replE find replace (splt a ','))):vys)
----------------------------------------------------------------------------------------------------
--funkce pro nahrazeni indexu n novym elmentem newEl v poli arr
replaceNth :: Int -> a -> [a] -> [a]
replaceNth n newEl arr =
    if(n==0)
      then
        [newEl] ++ (tail arr)
      else
        (take n arr) ++ [newEl] ++ (drop (n + 1) arr)
----------------------------------------------------------------------------------------------------
--obousmerne nahrazeni uzlu v mnozine uzlu
replE :: Eq a => a -> a -> [a] -> [a]
replE find r [] = []
replE find r (x:xs)=do
    if (find == x)
      then
        r : (replE find r xs)
      else
        if(r == x)
          then
            find : (replE find r xs)
          else
            x : (replE find r xs)
----------------------------------------------------------------------------------------------------
--
gs n [] vys = vys
gs n (p:ps) vys =
    gs n ps (replaceNth (convertStrToInt p) n vys)
----------------------------------------------------------------------------------------------------
--
genArrNodes [] [] vys = vys
genArrNodes (n:ns) (p:ps) vys =
    genArrNodes ns ps (gs n p vys)
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--jenoduche funkce
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--funkce pro rozdeleni retezce podle znaku
splt :: String -> Char -> [String]
splt str delim =
    let (start, end) = break (== delim) str
        in start : if null end then [] else splt (tail end) delim
----------------------------------------------------------------------------------------------------
--funkce pro ziskani casti pole v zadanem intervalu indexu
slice :: Int -> Int -> [a] -> [a]
slice from to [] = []
slice from to xs = take (to - from + 1) (drop from xs)
----------------------------------------------------------------------------------------------------
--funkce pro filtrovani pravidel z delta podle vychoziho(pocatecniho) stavu pravidla
-- zada se mnozina stavu
-- vysledkem jsou pravidla kde se tyto stavy nachazeji jako vychozi(vlevo)
filteRulesByStartNode :: [String] -> [String] -> [String] -> [String]
filteRulesByStartNode [] nodes vys = vys
filteRulesByStartNode (a:arr) nodes vys =
    if (isMember ((splt a ',')!!0) nodes)
      then
        filteRulesByStartNode arr nodes (a:vys)
      else
        filteRulesByStartNode arr nodes vys
----------------------------------------------------------------------------------------------------
--funkce pro filtrovani pravidel z delta podle stavu pravidla (vychoziho i ciloveho)
-- zada se mnozina stavu
-- vysledkem jsou pravidla kde se tyto stavy nachazeji jako vychozi(vlevo) i jako cilove(vpravo)
filteRules :: [String] -> [String] -> [String] -> [String]
filteRules [] nodes vys = vys
filteRules (a:arr) nodes vys =
    if ((isMember n nodes) && (isMember e nodes))
      then
        filteRules arr nodes (a:vys)
      else
        filteRules arr nodes vys
    where
      pom = (splt a ',')
      n=pom!!0
      s=pom!!1
      e=pom!!2
----------------------------------------------------------------------------------------------------
--funkce pro zjisteni Sigma z delta, tedy abecedy zadaneho KA
-- projde vsechny pravidla a poznamena si kazde pouzite pismeno v pravidlech prave jednou
exportSigma :: [String] -> [String] -> [String]
exportSigma [] vys = vys
exportSigma (d:delta) vys =
    --bez duplicit
    if not (isMember s vys)
      then
        exportSigma delta (s:vys)
      else
        exportSigma delta vys
    where
      --pismeno prechodu z aktualniho pravidla
      s = (splt d ',')!!1
----------------------------------------------------------------------------------------------------
--funkce pro porovnani cilovych stavu pravidel s n
compDest :: String -> [String] -> Bool
compDest n [] = False
compDest n (d:delta) =
    if (m == n)
      then
        compDest n delta
      else
        True
    where
      m = (splt d ',')!!2
----------------------------------------------------------------------------------------------------
--funkce pro porovnani mnoziny stavu
compNodes :: [String] -> [String] -> Bool
compNodes [] arr = False
compNodes arr [] = False
compNodes (a:as) (b:bs) =
    if((convertStrToInt a) == (convertStrToInt b))
      then
        compNodes as bs
      else
        False
----------------------------------------------------------------------------------------------------
--funkce pro detekci elementu e v poli
isMember :: String -> [String] -> Bool
isMember e [] = False
isMember e (a:arr) =
  if e == a
    then
      True
    else
      isMember e arr
----------------------------------------------------------------------------------------------------
--funkce pro vymazani duplicitnich vyskytu v poli
deleteDupl :: [String] -> [String] -> [String]
deleteDupl [] vys = vys
deleteDupl (d:dupl) vys =
    if (isMember d vys)
      then
        deleteDupl dupl vys
      else
        deleteDupl dupl (d:vys)
----------------------------------------------------------------------------------------------------
--funkce pro zjisteni spolecnych elemntu dvou poli
-- vrati pole elementu ktere jsou v prvnim i druhem poli
diffLists :: [String] -> [String] -> [String] -> [String]
diffLists [] scnd vys=vys
diffLists (f:fst) scnd vys=
    if (isMember f scnd)
      then
        diffLists fst scnd (f:vys)
      else
        diffLists fst scnd vys
----------------------------------------------------------------------------------------------------
--funkce pro zjisteni rozdilu prvniho a druheho pole
-- vrati pouze elementy prvniho pole ktere nejsou v tom druhem
diffL :: [String] -> [String] -> [String] -> [String]
diffL [] scnd vys=vys
diffL (f:fst) scnd vys=
    if (isMember f scnd)
      then
        diffL fst scnd vys
      else
        diffL fst scnd (f:vys)
----------------------------------------------------------------------------------------------------
--funkce pro vytvoreni novych pravidel
-- n startovni stav pravidel
-- sigma vsechny prechody, pro ktere ma byt pravidlo vytvoreno
-- n2 cilovy stav pravidel
newRules :: [[Char]] -> [Char] -> [[Char]] -> [[Char]] -> [[Char]]
newRules [] n n2 vys = vys
newRules (s:sigma) n n2 vys =
    newRules sigma n n2 ((n++","++s++","++(n2!!0)):vys)
----------------------------------------------------------------------------------------------------
--funkce pro scitani cisel
add :: Num a => a -> a -> a
add n1 n2 = n1+n2
----------------------------------------------------------------------------------------------------
--
cE [] nodes = True
cE (n:ns) nodes =
    if(isMember end nodes)
      then
        cE ns nodes
      else
        False
    where
      d = (splt n ',')
      end = d!!2
----------------------------------------------------------------------------------------------------
--
cM [] [] pk = True
cM (m:ms) (s:ss) pk =
    if(c1==c2 && mS==sS)
      then
        cM ms ss pk
      else
        False
    where
      (mE,mS) = m
      (sE,sS) = s
      c1=getInd mE pk 0
      c2=getInd sE pk 0
----------------------------------------------------------------------------------------------------
--funkce pro zjisteni indexu elementu v poli
getInd :: String->[[String]]->Integer->Integer
getInd e [] i = i
getInd e (a:as) i =
    if(isMember e a)
      then
        i
      else
        getInd e as c
    where
      c=i+1
----------------------------------------------------------------------------------------------------
--spojeni dvou poli do jednoho
convert2to1dArr :: [[a]] -> [a] -> [a]
convert2to1dArr [] vys = vys
convert2to1dArr (a:arr) vys =
    convert2to1dArr arr ((a!!0):vys)
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--prevadeci funkce mezi retezcem a cislem
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--prevedeni cisla na retezec
convertIntToStr :: [Int] -> [String]
convertIntToStr = (>>= return.show)
----------------------------------------------------------------------------------------------------
--prevedeni retezce na cislo
convertStrToInt :: String -> Int
convertStrToInt s = read s::Int
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--funkce pro zjisteni a sjednoceni nerozlisitelnych stavu
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--funkce pro postupne prochazeni jednotlivych mnozin v mnozine nerozlisitelnych stavu
dis :: (Eq a, Num a) => [String] -> [[String]] -> [String] -> [(String, String)]
                      -> a -> [[Char]] -> [[Char]] -> [[Char]] -> [[[Char]]]
dis delta pk pl mgo mgoNum [] p1 p2 = [(sortBy sortNodes p1), (sortBy sortNodes p2)]
dis delta pk pl mgo mgoNum (p:ps) p1 p2 =
    if(cE eN pl)
      then
        if(mgoNum==1)
          then
            if(cM mgo eN2 pk)
              then
                dis delta pk pl mgo mgoNum ps (p:p1) p2
              else
                dis delta pk pl mgo mgoNum ps p1 (p:p2)
          else
            dis delta pk pl mgo mgoNum ps (p:p1) p2
      else
        if(mgoNum==2)
          then
            if(cM mgo eN2 pk)
              then
                dis delta pk pl mgo mgoNum ps p1 (p:p2)
              else
                dis delta pk pl mgo mgoNum ps (p:p1) p2
          else
            dis delta pk pl mgo mgoNum ps p1 (p:p2)
    where
      eN = filteRulesByStartNode delta [p] []
      eN2 = gOut pl eN []
----------------------------------------------------------------------------------------------------
--funkce pro nalezeni nasledujici mnoziny nerozlisitelnych stavu
disting :: [[String]] -> [[String]] -> [String] -> [[[Char]]] -> [[[Char]]]
disting pk [] delta vys = vys
disting pk (p:ps) delta vys =
    disting pk ps delta (v++vys)
    where
      v = if(cE eN p)
            then
              dis delta pk p (gOut p eN []) 1 (slice 1 (length(p)) p) [p!!0] []
            else
              dis delta pk p (gOut p eN []) 2 (slice 1 (length(p)) p) [] [p!!0]

      eN = filteRulesByStartNode delta [p!!0] []
----------------------------------------------------------------------------------------------------
--funkce pro nalezeni mnozin nerozlisitelnych stavu
distinguished :: [String] -> [[String]] -> [[String]]
distinguished automat pk1=do
    let (nodes,startNode,endNodes,delta) = (parse automat)

    let pk2 = sortBy sortByFirstNode (filter (not . null) (disting pk1 pk1 delta []))

    if(pk2 == pk1)
      then
        pk2
      else
        distinguished automat pk2
----------------------------------------------------------------------------------------------------
--funkce pro zjisteni pres ktere prechody a do kterych stavu jde zadany uzel z mnoziny ven
gOut :: [String] -> [String] -> [(String, String)] -> [(String, String)]
gOut p [] vys = vys
gOut p (d:ds) vys =
    if(isMember end p)
      then
        gOut p ds vys
      else
        gOut p ds ((end,s):vys)
    where
      r = (splt d ',')
      s = r!!1
      end = r!!2
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--funkce pro nacteni a premapovani KA
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--funkce pro prochazeni delta podle posledniho pravidla formatu vystupu
-- zaruci ze napravo zadneho pravidla nebude stav o dva vetsi nez predchazejici pravidla
walkDelta :: [String] -> Int -> ([Char], [Char])
walkDelta [] max = ([], [])
walkDelta (d:delta) max =
    if((max+2) > a)
      then
        if (a>max)
          then
            walkDelta delta a
          else
            walkDelta delta max
      else
        (((convertIntToStr [a])!!0),((convertIntToStr [(max+1)])!!0))
    where
      a=(convertStrToInt ((splt d ',')!!2))
----------------------------------------------------------------------------------------------------
--nacteni zadaneho KA do rozdelene verze
-- vsechny mnoziny jsou samostatne oddelitelne
parse :: [String] -> ([[Char]], String, [[Char]], [[Char]])
parse automat=do
    let nodes = sortBy sortNodes (splt (automat!!0) ',')
    let startNode = (automat!!1)
    let endNodes = sortBy sortNodes (splt (automat!!2) ',')
    let delta = sortBy sortDelta (slice 3 (length(automat)) automat)

    (nodes,startNode,endNodes,delta)
----------------------------------------------------------------------------------------------------
--predelani rozdeleneho formatu KA do jednoho uceleneho
-- pro snadnejsi predavani celeho KA a zaverecny vystup
parseN :: [[Char]] -> [[Char]] -> [[Char]] -> [[Char]]
parseN nodes endNodes delta=do
    let automat = (intercalate "," nodes) : ("0":[]) ++ [(intercalate "," endNodes)] ++ (delta)

    automat
----------------------------------------------------------------------------------------------------
--obousmerne premapovani pravidel a koncovych stavu
remapDelta :: [[Char]] -> [[Char]] -> ([String], [[Char]])
remapDelta delta endNodes = do
    if((length rep) > 0)
      then
        remapDelta delta2 (sortBy sortNodes (replE old rep endNodes))
      else
        (delta,endNodes)
    where
      max1 = (convertStrToInt ((splt (delta!!0) ',')!!2))
      (old,rep) = (walkDelta delta max1)
      delta2 = (sortBy sortDelta (replFSe old rep delta []))
----------------------------------------------------------------------------------------------------
--premapovani KA s pouziti mnoziny pk ktera obsahuje mnoziny nerozlisitelnych stavu
remapDistinguished :: [String] -> [[String]] -> [[Char]]
remapDistinguished automat pk=do
    let (nodes,startNode,endNodes,delta) = (parse automat)
    let sigma = (exportSigma delta [])

    let deltaR = sortBy sortDelta delta

    let nodesDist = convertIntToStr [0 .. ((length pk)-1)]
    let nodesDistR = genArrNodes nodesDist pk (replicate (length nodes) "-1")
    let endNodesDist = sortBy sortNodes (deleteDupl (replF nodes nodesDistR endNodes []) [])
    let deltaDist = sortBy sortDelta (deleteDupl (replFS (sigma++nodes) (sigma++nodesDistR) deltaR []) [])

    let (deltaDistM,endNodesDistM) = remapDelta deltaDist endNodesDist

    parseN nodesDist endNodesDistM deltaDistM
----------------------------------------------------------------------------------------------------
--premapovani KA podle pravidel na vystup
remap :: [String] -> [[Char]]
remap automat = do
    let (nodes,startNode,endNodes,delta) = (parse automat)
    let sigma = (exportSigma delta [])

    let sN=(not (startNode == nodes!!0))
    let sNi=startNode

    -- osetreni pocatecniho stavu
    let endNodesS = if(sN)
        then
          sortBy sortNodes (replE sNi "0" endNodes)
        else
          endNodes

    let deltaS = if(sN)
        then
          sortBy sortDelta (replFSe sNi "0" delta [])
        else
          delta

    let nodesS=if(sN)
        then
          sortBy sortNodes (replE sNi "0" nodes)
        else
          nodes

    -- premapovani stavu
    let nodesR = convertIntToStr [0 .. ((length nodesS)-1)]
    let endNodesRs = sortBy sortNodes (replF nodesS nodesR endNodesS [])
    let deltaRs = sortBy sortDelta (replFS (sigma++nodesS) (sigma++nodesR) deltaS [])

    -- premapovani delty podle posledniho pravidla zadani
    let (deltaRM,endNodesRM) = remapDelta deltaRs endNodesRs

    let automatR = parseN nodesR endNodesRM deltaRM

    automatR
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--funkce pro zjisteni nedosazitelnych, sink stavu
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--funkce pro zjisteni toho zda existuje cesta ze zadaneho uzlu
existWayFrom :: String -> [String] -> Bool
existWayFrom node delta=do
    let deltaN = (filteRulesByStartNode delta [node] [])

    if((((length deltaN) > 0) && (compDest node deltaN)))
      then
        True
      else
        False
----------------------------------------------------------------------------------------------------
--funkce pro detekci sink stavu
-- vrati vsechny sink stavy v poli,
-- na zakldae pravidel a koncovych stavu
sinkNodes :: [String] -> [String] -> [String] -> [String] -> [String]
sinkNodes delta endNodes [] vys=vys
sinkNodes delta endNodes (n:nodes) vys =
    if ((isMember n endNodes) || (existWayFrom n delta))
      then
        sinkNodes delta endNodes nodes vys
      else
        sinkNodes delta endNodes nodes (n:vys)
----------------------------------------------------------------------------------------------------
--funkce pro zjisteni dosazitelnych stavu
-- jeden krok
-- na zacatku je nutne dat do vys pocatecni stav
reachableNod :: [String] -> [String] -> [String]
reachableNod [] vys = vys
reachableNod (a:arr) vys =
    if((isMember n1 vys) && (not (n1 == n2)))
      then
        reachableNod arr (n2:vys)
      else
        reachableNod arr (vys)
    where
      n1 = ((splt a ',')!!0)
      n2 = ((splt a ',')!!2)
----------------------------------------------------------------------------------------------------
--funkce pro zjisteni dosazitelnych stavu
-- ridi cely proces nalezeni mnoziny vsech dosazitelnych stavu
-- na zacatku je nutne dat do vys pocatecni stav
reachableNodes :: [String] -> [String] -> [String]
reachableNodes delta vys= do
    let a=(deleteDupl (reachableNod delta vys) [])
    let b=(deleteDupl (reachableNod delta a) [])

    if(a == b)
      then
        b
      else
        reachableNodes delta b
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--vymazavaci funkce
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--funkce pro odstraneni nedostupnych stavu
-- zachova pouze dosazitelne stavy ze zadane mnoziny
rmUnreachNodes :: [String] -> [String] -> [String] -> ([String], [[Char]], [[Char]])
rmUnreachNodes rchNods endNodes delta = do
    let nodesN = rchNods
    let endNodesN = sortBy sortNodes (diffLists endNodes nodesN [])
    let deltaN = sortBy sortDelta (filteRulesByStartNode delta nodesN [])

    (nodesN,endNodesN,deltaN)
----------------------------------------------------------------------------------------------------
--funkce pro postupne odstraneni sink stavu
-- sinkNods obsahuje na konci vsechny smazane sink stavy
rmSinkNodes :: ([[Char]], [[Char]], [[Char]]) -> [[String]] -> ([String], [String], [String])
rmSinkNodes ned sinkNods= do
    let (nodes,endNodes,delta)=ned
    let s=sinkNodes delta endNodes nodes []

    if((length s) > 0)
      then
        rmSinkNodes (removeNodes s nodes endNodes delta) (s:sinkNods)
      else
        (nodes,endNodes,delta)
----------------------------------------------------------------------------------------------------
--hromadne odstraneni uzlu ze zadanych mnozin za pouziti prislusnych funkci pro danou mnozinu
removeNodes :: [String] -> [String] -> [String] -> [String] -> ([[Char]], [[Char]], [[Char]])
removeNodes rmN nodes endNodes delta = do
    let nodesN = sortBy sortNodes (diffL nodes rmN [])
    let endNodesN = sortBy sortNodes (diffLists endNodes nodesN [])
    let deltaN = sortBy sortDelta (filteRules delta nodesN [])

    (nodesN,endNodesN,deltaN)
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--funkce pro totalni prechodovou funkci delta
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--doplneni delta na totalni prechodovou funkci pro pravidla zacinajicimi uzlem n
-- n2 je sink stav pro doplneni
dt :: String -> [String] -> [String] -> [String]
dt n n2 delta=do
    let sigma=sort (exportSigma delta [])

    let deltaN=filteRulesByStartNode delta [n] []
    let sigmaN=sort (exportSigma deltaN [])

    let sigmaDF=diffL sigma sigmaN []

    if((length sigmaDF)>0)
      then
        newRules sigmaDF n n2 []
      else
        []
----------------------------------------------------------------------------------------------------
--doplneni delta na totalni prechodovou funkci
-- n2 je sink stav pro doplneni
deltaTotal :: [String] -> [String] -> [String] -> [String]
deltaTotal (n:nodes) n2 deltaT = do
    if((length nodes)>0)
      then
        deltaTotal nodes n2 (deltaT++deltaN)
      else
        (deltaT++deltaN)
    where
      deltaN = dt n n2 deltaT
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--hlavni ridici funkce celeho procesu
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--funkce pro minimalizaci konecneho automatu na MKA s totalni prechodovou funkci
minimalization :: [String] -> IO ()
minimalization automatR = do
    let (nodes,startNode,endNodes,delta) = parse automatR

  -- odstrani nedosazitelne stavy
    let rchNods = sortBy sortNodes (reachableNodes delta ["0"])
    let (nodesM,endNodesM,deltaM) = (rmUnreachNodes rchNods endNodes delta)

  -- doplnim na totalni prechodovou funkci
    let nS = convertIntToStr [(add (length nodesM) 2)]
    let deltaT=sortBy sortDelta (deltaTotal nodesM nS deltaM)

  -- pokud byl pridan sink stav tak jej pridej i do vsech stavu a jeho pravidla
    let tot = (deltaM==deltaT)
    let nodesM2 = if(tot)
                    then
                      nodesM
                    else
                      nodesM++nS

    let deltaTot = if(tot)
                    then
                      deltaT
                    else
                      (newRules (exportSigma deltaT []) (nS!!0) nS [])++deltaT

    -- automat s totalni prechodovou funkci delta, bez nedosazitelnych stavu
    let automatTKA = remap (parseN nodesM2 endNodesM deltaTot)
    -- putStr (unlines automatTKA)
  -- sjednoceni nerozlisitelnych stavu
    let (nodesMT,startNodeMT,endNodesMT,deltaMT) = parse automatTKA
    let dis = distinguished automatTKA (sortBy sortByFirstNode [endNodesMT, (sort (diffL nodesMT endNodesMT []))])
    -- print(dis)
    -- minimalni konecny automat s totalni prechodovou funkci
    let automatMTKA = remapDistinguished automatTKA dis

    {-
  -- odstraneni sink stavu
    let sinkNods = []
    let (nodesS,endNodesS,deltaS) = (rmSinkNodes (nodesM2,endNodesM,deltaTot) sinkNods)
    let (nodesSr,endNodesSr,deltaSr) = parse (remap (parseN nodesS endNodesS deltaS))

    -- minimalni konecny automat
    let automatMKA = parseN nodesSr endNodesSr deltaSr
    -}

    putStr (unlines automatMTKA)
----------------------------------------------------------------------------------------------------
--zpracovani vstupu a hlavni vypis
main :: IO ()
main = do
    args <- getArgs

    --osetreni poctu argumentu
    let lenArg=length args
    if ((lenArg > 2 || (lenArg < 1)) && (lenArg<4))
      then
        invalidArg
      else do
        let content = if (lenArg > 1)
                        then do
                          let file=(args!!1)
                          filExist <- (doesFileExist file)

                          if (filExist)
                            then
                              readFile file
                            else
                                fileNotExist file

                        else
                            getContents

        --nacteni obsahu souboru s DKA
        contentS <- content

        --rozparsovani nacteneho DKA podle radku
        let automat = lines contentS

        --premapovani vstupniho DKA automatu
        let automatR = remap automat

        --overeni jestli mam minimalizovat
        if (args!!0 == "-i")
          then
            --pouze vypisu premapovany automat
            putStr (unlines automatR)
          else if(args!!0 == "-t")
            then
              --minimalizace vstupniho DKA na MKAtot a jeho vypis
              minimalization automatR
            else
              invalidArg
