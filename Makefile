#FLP2019fun, dka-mka
#autor: Zdenek Sklenar, xsklen09

all:build

build:
	ghc -o dka-2-mka ./dka-2-mka.hs

run: build
	./dka-2-mka -t ./tests/test2.in

clean:
	rm -f dka-2-mka dka-2-mka.o dka-2-mka.hi